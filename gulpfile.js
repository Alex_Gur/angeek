var gulp = require('gulp');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var runSequence = require('gulp4-run-sequence');
var rename      = require('gulp-rename');
var buffer = require('gulp-buffer');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var wrap = require('gulp-wrap');
var path = require('path');
var htmlbeautify = require('gulp-html-beautify');
var autoprefixer = require('gulp-autoprefixer');

// Sass
gulp.task('sass', function () {
  gulp.src('src/assets/stylesheets/*.scss')
    .pipe(sass())
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(gulp.dest('./public/assets/stylesheets'));
  return new Promise(function(resolve, reject) {
    resolve();
  });
});

// pug
gulp.task('pug', function(){
  return gulp.src('src/pages/*.pug')
    .pipe(pug())
    .pipe(htmlbeautify({
      indentSize: 4,
      unformatted: [
        'abbr', 'area', 'b', 'bdi', 'bdo', 'br', 'cite', 'code', 'data', 'datalist', 'del', 'dfn', 'em', 'embed', 'i', 'ins', 'kbd', 'keygen', 'map', 'mark', 'math', 'meter', 'noscript', 'object', 'output', 'progress', 'q', 'ruby', 's', 'samp', 'small', 'strong', 'sub', 'sup', 'template', 'time', 'u', 'var', 'wbr', 'text', 'acronym', 'address', 'big', 'dt', 'ins', 'strike', 'tt'
    ]
    }))
    .pipe(gulp.dest('public/'));
});

// Concat all JS-plugins
gulp.task('concatJs', function() {
	return gulp.src(['./src/assets/scripts/*'])
	.pipe(wrap('\n//<%= file.path %>\n//-->\n<%= contents %>'))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('./public/assets/scripts/'));
});

// Copy JS
gulp.task('copyJs', function() {
    gulp.src(['./src/assets/scripts/vendor/jquery/*'])
    .pipe(gulp.dest('./public/assets/scripts/jquery/'));
    return new Promise(function(resolve, reject) {
      resolve();
    });
});

// Copy Images
gulp.task('copyImages', function() {
    gulp.src('./src/resources/images/**/*')
    .pipe(gulp.dest('./public/assets/images/'));
    return new Promise(function(resolve, reject) {
      resolve();
    });
});

// Copy temporary pictures
gulp.task('copyTempPics', function() {
    gulp.src('./src/resources/temp/**/*')
    .pipe(gulp.dest('./public/temp/'));
    return new Promise(function(resolve, reject) {
      resolve();
    });
});

/// Delete the contents of all first level folders of './public' folder
// except html-files

gulp.task('clean', function () {
    return gulp.src(['./public/assets/*', './public/temp/*', './public/*.html',], {read: false})
    .pipe(clean({force: true}));
});


// Watch taskes
gulp.task('watch', function() {
  gulp.watch(['src/assets/stylesheets/*.scss',
            'src/assets/stylesheets/**/*.scss',
            'src/assets/stylesheets/**/**/*.scss',
            'src/templates/**/*.scss'], gulp.series('sass'));
  gulp.watch(['src/pages/*.pug',
            'src/pages/**/*.pug',
            'src/templates/**/*.pug'], gulp.series('pug'));
  gulp.watch(['src/assets/scripts/*.*'], gulp.series('concatJs')); //
  gulp.watch(['./src/assets/scripts/vendor/jquery/*.*'], gulp.series('copyJs')); //
  gulp.watch(['src/resources/images/*.*', 'src/resources/images/**/*.*'], gulp.series('copyImages'));
  gulp.watch(['src/resources/temp/*.*', 'src/resources/temp/**/*.*'], gulp.series('copyTempPics'));
});

gulp.task('default', function(callback) {
  runSequence( 'clean',
              'sass',
              ['pug',
               'concatJs',
               'copyJs',
               'copyImages',
               'copyTempPics'],
              'watch',
               callback);
});
